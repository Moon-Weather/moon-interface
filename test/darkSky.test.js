const { of } = require('rxjs');
const { take } = require('rxjs/operators');
const { TestScheduler } = require('rxjs/testing');
const nock = require('nock');

const {createApiUrlObs, requestDarkSky, timeDarkSkyRequest} = require('../darkSky');

const key = "yiduh853hudissead";
const geo = "52.3848366,4.9290483";
const expectedApiUrl = `https://api.darksky.net/forecast/${key}/${geo}`;

const assertDeepEqual = (actual, expected) => expect(actual).toEqual(expected);

const mockResponse = {
    latitude: '37.8267',
    longitude: '-122.4233',
    hourly: {
        summary: "Partly cloudy starting tomorrow evening."
    }
};

nock('https://api.darksky.net')
    .get(`/forecast/${key}/${geo}`)
    .reply(200, mockResponse);

describe('DarkSky', () => {
    describe('make API request', () => {

        let scheduler;

        beforeEach(() => {
            scheduler = new TestScheduler(assertDeepEqual);
        });

        it('outputs the correct api url', () => {
            const darkSky$ = createApiUrlObs({key, geo});

            darkSky$.subscribe(value => expect(value).toEqual({url: expectedApiUrl}));
        });

        it('responds with weather data', (done) => {
            const darkSky$ = requestDarkSky({url: expectedApiUrl});

            darkSky$.subscribe(value => {
                expect(value).toEqual(mockResponse);
                done();
            });
        });

        it('trigger a new request for data every x amount of time', () => {
            scheduler.run(helpers => {
                const darkSky$ = timeDarkSkyRequest({time: 2});
                const darkSkyTest$ = darkSky$.pipe(take(3));
                const expected = '0-2-(4|)';

                helpers.expectObservable(darkSkyTest$).toBe(expected, {0: 0, 2: 1, 4: 2});
            });
        });
    });
});
