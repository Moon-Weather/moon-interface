const {of, timer, from} = require('rxjs');
const {switchMap, flatMap, shareReplay} = require('rxjs/operators');
const fetch = require('node-fetch');

/**
 * Create DarkSky api url
 * @param key
 * @param geo
 */
const createApiUrl = ({key, geo}) => `https://api.darksky.net/forecast/${key}/${geo}`;

/**
 * Create an api url observable
 * @param key
 * @param geo
 */
const createApiUrlObs = ({key, geo}) => of({ url: createApiUrl({key, geo}) });

/**
 * Request data from DarkSky api
 * @param url
 */
const requestDarkSky = ({url}) => from(fetch(url)).pipe(
    switchMap(response => response.json())
);

/**
 * Run request for new data every x seconds
 * @param time
 */
const timeDarkSkyRequest = ({time}) => timer(0, time);

/**
 * Start darkSky calls every x seconds
 * @param key
 * @param geo
 * @param time
 */
const initDarkSky = ({key, geo, time}) => {
    const url$ = createApiUrlObs({key, geo});
    const request$ = url$.pipe(switchMap(requestDarkSky));
    const timer$ = timeDarkSkyRequest({time});

    return timer$.pipe(
        flatMap(() => request$),
        shareReplay(1)
    );
};

module.exports = {
    initDarkSky,
    createApiUrlObs,
    requestDarkSky,
    timeDarkSkyRequest
};