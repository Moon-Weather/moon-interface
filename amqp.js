const amqp = require('amqplib/callback_api');

/**
 * Connect to amqp channel and supply helper functions
 * @param host
 * @param ex
 * @param app
 * @param callback
 */
const connect = (host, ex, app, callback) => {
    amqp.connect(host, (err, conn) => {
        conn.createChannel((err, ch) => {
            ch.assertExchange(ex, 'topic', {durable: true});

            /**
             * Create a queue
             * @param q
             * @param key
             * @param callback
             */
            const assertQueue = (q, key, callback) => {
                ch.assertQueue(q, {exclusive: true}, (err, q) => {
                    ch.bindQueue(q.queue, ex, key);

                    console.log(` [*] Waiting for messages on ${key}`);

                    ch.consume(q.queue, ({content, fields: { routingKey }}) => {

                        const parsedContent = JSON.parse(content);

                        consumeLogger(routingKey, content);

                        callback(parsedContent);

                    }, {noAck: true});
                })
            };

            /**
             * Send the intent to apps (ex. Moes or Calendar api)
             * @param message
             * @param key
             */
            const sendMessage = (message, key) => {
                ch.publish(ex, key, new Buffer(JSON.stringify({
                    ...message,
                    app
                })));

                console.log(" [x] => %s:'%s'", key, JSON.stringify(message));
            };

            /**
             * Log messages
             * @param routingKey
             * @param message
             */
            const consumeLogger = (routingKey, message) => console.log(" [x] <= %s:'%s'", routingKey, message);

            /**
             * Trigger callback
             */
            callback({assertQueue, sendMessage, err});
        });
    });
};

module.exports = connect;