const connect = require('./amqp');
const {initDarkSky} = require('./darkSky');
const {first} = require('rxjs/operators');

const APP = 'moon';
const KEY_INTENT = `rabot.apps.intents.${APP}`;
const KEY_RESPONSE = `rabot.apps.responses.${APP}`;

const darkSkyStream$ = initDarkSky({
    key: process.env.DARKSY_API_KEY,
    geo: "52.3848366,4.9290483",
    time: 10000
});

darkSkyStream$.subscribe(weatherData => {
    console.log("darkSkyStream$ => to topic channel..", weatherData.hourly.summary)
});

const darkSkyFromMemory$ = darkSkyStream$.pipe(first());

connect(process.env.RABBIT_MQ, 'amq.topic', APP, ({assertQueue, sendMessage}) => {
    /**
     * USER => APP: Listen to utterances
     */
    assertQueue(
        'moon.intent',
        KEY_INTENT,
        (message) => {
            const {username, intent} = message;

            darkSkyFromMemory$.subscribe(weatherData => {
                sendMessage({
                    utterance: getFeedbackOnIntent(intent, weatherData),
                    replyTo: message
                }, KEY_RESPONSE, username)
            });
        }
    );
});

/**
 * Decide how to reply to the intent
 * @param intent
 * @param daily
 * @param hourly
 */
function getFeedbackOnIntent(intent, {daily, hourly}) {
    switch (intent) {
        case "today":
            return hourly.summary;
            break;
        case "weekend":
            return daily.summary;
            break;
        default:
            return "Sorry, I can not answer that";
    }
}